# /usr/bin/env python3
# Avi Graber, F5 Networks, 2024-Jan-5
#
# This tool automatically checks out and checks in each WAF policy for a given customer account/ID. For WAF policies that haven't been checked in since 2020 or before, the most recent XML file does not have the following learning modules: learning_mode, learn_inactive_entities, and enable_full_policy_inspection. As a result, we cannot edit the policy XML to disable them. The solution is to check out these policies and check them in so that the XML file is up-to-date and consequently contains the aforementioned learning modules. At that point, we can use the disabler script in this repository to disable the learning modules.

__author__ = "Avi Graber"
__date__ = "2024-Jan-5"
__version__ = "1.0"

import traceback
import requests
import datetime
import pandas as pd
import time
import sys
import os
import argparse
import urllib3
import getpass

# supressing SSL Warning from Policy Manager API cert
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
custId = input('Please type the customer ID (case-sensitive) and hit enter: ')
policyCheckedOutCheck = input(f"Have you confirmed that no {custId} WAF policies are currently checked out to either WAF editor (we17018.lon1 and we18018.lon1)? ").lower()
if policyCheckedOutCheck != 'y' and policyCheckedOutCheck != 'yes':
    sys.exit(f"Please confirm that no {custId} WAF policies are currently checked out to either WAF editor (we17018.lon1 and we18018.lon1) before proceeding with this task.")
# getpass library promotes security. The apiToken is not visible in the source code nor is it visible if someone were watching your terminal screen over your shoulder (shoulder surfing)
apiToken = getpass.getpass('Please copy-paste one of the customer\'s Portal API token values and hit enter: ')
# Example portalUser is "John Doe"
portalUser = getpass.getpass("Please type your portal username (case-sensitive) and hit enter (typically the format is Firstname Lastname): ")
date = str(datetime.datetime.today().date())
filenameCsv = f"./{custId}_check_in_check_out_summary_{date}.csv"

def get_customer_policies():
    # retrieves list of WAF Policies from Portal API for customer corresponding to the API token used
    print(f"\nGetting List of Policies for {custId}\n")
    custPolicies = []
    page = 1
    while page == 1 or len(custPolicies) % 200 == 0:
        portalApiUri = f'https://portal.f5silverline.com/api/v1/waf_policies?page={page}'
        headers = {
            'Content-Type': 'application/json',
            'X-Authorization-Token': apiToken,
        }
        responsePortal = requests.get(portalApiUri, headers=headers, verify=False, timeout=10)
        if responsePortal.status_code != 200:
            sys.exit("Request to Portal API failed. Verify that your API token is valid.")
        dataPortal = responsePortal.json()
        currentPolicies = dataPortal["data"]["waf_policies"]
        if len(currentPolicies) == 0:
            break
        for policy in currentPolicies:
            # The aforementioned Silverline Portal API endpoint contains only current WAF policies for the customer.
            custPolicies.append(policy)
        page += 1
    return custPolicies

def check_out_policy(policyName, policyPortalId):
    print(f'\nChecking out policy {policyName} ...')
    headers = {
        'Content-Type': 'application/json',
        'X-Originator': 'customer-portal',
    }
    json_data = {
        'originator': f'portal-ui:{portalUser}',
        'job_meta': {
            'deploy_uri': f'https://portal-vip-bim.sjc1.defense.net/api/v1/admin/customers/{custId}/waf_policies/{policyPortalId}/checkout',
            'retriable': False,
        },
        'waf-checkout': {
            'name': policyName,
            'uri': f'https://api.sjc1.defense.net/api/wipes/v2/policies/{policyName}/xml-by-name',
            'bigip_version': 14,
            'customer_id': custId,
            'edit_region': 'lon',
        },
        'pass_thru': {
            'customer_id': custId,
            'waf_policy_id': policyPortalId,
        },
        'beta_features': [
            'waf_policy_self_service',
            'l7_profiles_v3',
            'use_customer_socket_address_list',
            'corero_edge_stats',
            'target_host_optimization',
            'jobs_retriable',
            'mutual_tls',
            'wao_host_filter',
            'wao_multiple_fqdns',
            'classic_proxy_viewable',
            'bot_defense_lite',
            'shape_defense_insert_device_id',
            'dcm',
            'http2',
            'task_skipping',
            'irule_template',
            'web_applications',
            'wao_ipv6_support',
            'weblogs',
            'proxy_monitors_history',
            'log_export_v2',
            'policy_manager',
            'http_redirect_at_tier_1',
            'violation_summary_v2',
        ],
    }
    try:
        response = requests.post(
            'http://api.ss1-pdx.defense.net/api/v1/job-manager/jobs/waf-policy/check-out',
            headers=headers,
            json=json_data,
            verify=False,
        )
    except requests.exceptions.Timeout:
        sys.exit("Request to Job Manager API timed out. Are you connected to the VPN?")
    # Avoid overwhelming Job manager
    time.sleep(15)
    response = response.json()
    checkOutStatusUri = response["job_status_uri"]
    print(f'Check Out Job Status URI: {checkOutStatusUri}\n')
    return checkOutStatusUri

def check_in_policy(policyName, policyPortalId):
    print(f'\nChecking in policy {policyName} ...')
    headers = {
        'Content-Type': 'application/json',
        'X-Originator': 'customer-portal',
    }
    json_data = {
        'originator': f'portal-ui:{portalUser}',
        'job_meta': {
            'deploy_uri': f'https://portal-vip-bim.sjc1.defense.net/api/v1/admin/customers/{custId}/waf_policies/{policyPortalId}/checkout',
            'retriable': False,
        },
        'waf-checkin': {
            'name': policyName,
            'uri': f'https://api.sjc1.defense.net/api/wipes/v2/policies/{policyName}/xml',
            'user': f'portal-ui:{portalUser}',
            'customer_id': custId,
            'comment': 'Ensuring XML is up-to-date for policy learning disabling project'
            # 'phase': 'blocking_other',
        },
        'pass_thru': {
            'customer_id': custId,
            'waf_policy_id': policyPortalId,
        },
        'beta_features': [
            'waf_policy_self_service',
            'l7_profiles_v3',
            'use_customer_socket_address_list',
            'corero_edge_stats',
            'target_host_optimization',
            'jobs_retriable',
            'mutual_tls',
            'wao_host_filter',
            'wao_multiple_fqdns',
            'classic_proxy_viewable',
            'bot_defense_lite',
            'shape_defense_insert_device_id',
            'dcm',
            'http2',
            'task_skipping',
            'irule_template',
            'web_applications',
            'wao_ipv6_support',
            'weblogs',
            'proxy_monitors_history',
            'log_export_v2',
            'policy_manager',
            'http_redirect_at_tier_1',
            'violation_summary_v2',
        ],
    }
    try:
        response = requests.post(
            'http://api.ss1-pdx.defense.net/api/v1/job-manager/jobs/waf-policy/check-in',
            headers=headers,
            json=json_data,
            verify=False,
        )
    except requests.exceptions.Timeout:
        sys.exit("Request to Job Manager API timed out. Are you connected to the VPN?")
    # Avoid overwhelming job manager
    time.sleep(15)
    response = response.json()
    checkInStatusUri = response["job_status_uri"]
    print(f'Check In Job Status URI: {checkInStatusUri}\n')
    return checkInStatusUri

def check_out_status_check(checkOutInSummary):
    # Check the most recent 10 check out jobs
    numberFinalPolicies = len(checkOutInSummary['Policy']) % 10
    if numberFinalPolicies == 0:
        # batch of 10
        checkedOutStatuses = checkOutInSummary["Check Out Status"][-10:]
        checkedOutStatusUris = checkOutInSummary['Check Out Job Status URI'][-10:]
    else:
        # not a batch of 10, checking the last batch of policies
        checkedOutStatuses = checkOutInSummary["Check Out Status"][(-10 + (10 - numberFinalPolicies)):]
        checkedOutStatusUris = checkOutInSummary['Check Out Job Status URI'][(-10 + (10 - numberFinalPolicies)):]
    start = time.perf_counter()
    finish = start
    # checkedOutStatusUris contains 10 policy check out job status URI's
    while int(finish - start) < 900 and "IN PROGRESS" in checkedOutStatuses:
        for index, checkedOutStatusUri in enumerate(checkedOutStatusUris):
            if checkedOutStatuses[index] != "IN PROGRESS":
                continue
            try:
                response = requests.get(checkedOutStatusUri, verify=False, timeout=10)
            except requests.exceptions.Timeout:
                sys.exit("Request to Job Manager API timed out. Are you connected to the VPN?")
            response = response.json()
            if response["intention"]["success"] == True and response["intention"]["complete"] == True and response["intention"]["status"] == "complete":
                checkedOutStatuses[index] = "Completed"
            elif response["intention"]["success"] == False and response["intention"]["complete"] == True and response["intention"]["status"] == "failed":
                checkedOutStatuses[index] = "FAILED. Request the job status URI to investigate further."
        # Wait 10 seconds before checking job statuses again
        time.sleep(5)
        print(f"{checkedOutStatuses.count('Completed')} policies have successfully checked out.")
        print(f"{checkedOutStatuses.count('IN PROGRESS')} policies are still attempting to check out.")
        print(f"{checkedOutStatuses.count('FAILED. Request the job status URI to investigate further.')} policies have failed to check out.")
        time.sleep(5)
        finish = time.perf_counter()
        print(f'The check out operation has taken {int(finish - start)} seconds so far.\n')
    if numberFinalPolicies == 0:
        # batch of 10
        checkOutInSummary["Check Out Status"][-10:] = checkedOutStatuses
    else:
        # not a batch of 10, checking the last batch of policies
        checkOutInSummary["Check Out Status"][(-10 + (10 - numberFinalPolicies)):] = checkedOutStatuses
    return checkOutInSummary

def check_in_status_check(checkOutInSummary):
    # Check the most recent jobs
    # This is if the batch isn't a clean 10, such as the last 3 policies if you had 103 policies in the account.
    numberFinalPolicies = len(checkOutInSummary['Policy']) % 10
    if numberFinalPolicies == 0:
        # batch of 10
        checkedInStatuses = checkOutInSummary["Check In Status"][-10:]
        checkedInStatusUris = checkOutInSummary['Check In Job Status URI'][-10:]
    else:
        # not a batch of 10, checking the last batch of policies
        checkedInStatuses = checkOutInSummary["Check In Status"][(-10 + (10 - numberFinalPolicies)):]
        checkedInStatusUris = checkOutInSummary['Check In Job Status URI'][(-10 + (10 - numberFinalPolicies)):]
    # Check the most recent 10 check in jobs
    start = time.perf_counter()
    finish = start
    # checkedInStatusUris contains up to 10 policy check in job status URI's
    while int(finish - start) < 900 and "IN PROGRESS" in checkedInStatuses:
        for index, checkedInStatusUri in enumerate(checkedInStatusUris):
            if checkedInStatuses[index] != "IN PROGRESS":
                # Status is either "Completed", "FAILED. Request the job status URI to investigate further.", or "Not attempted because policy failed check out."
                continue
            try:
                response = requests.get(checkedInStatusUri, verify=False, timeout=10)
            except requests.exceptions.Timeout:
                sys.exit("Request to Job Manager API timed out. Are you connected to the VPN?")
            response = response.json()
            if response["intention"]["success"] == True and response["intention"]["complete"] == True and response["intention"]["status"] == "complete":
                checkedInStatuses[index] = "Completed"
            elif response["intention"]["success"] == False and response["intention"]["complete"] == True and response["intention"]["status"] == "failed":
                checkedInStatuses[index] = "FAILED. Request the job status URI to investigate further."
        # Wait 10 seconds before checking job statuses again
        time.sleep(5)
        print(f"{checkedInStatuses.count('Completed')} policies have successfully checked in.")
        print(f"{checkedInStatuses.count('IN PROGRESS')} policies are still attempting to check in.")
        print(f"{checkedInStatuses.count('FAILED. Request the job status URI to investigate further.')} policies have failed to check in.")
        print(f"{checkedInStatuses.count('Not attempted because policy failed check out.')} policies have not attempted to check in because they failed to check out.")
        time.sleep(5)
        finish = time.perf_counter()
        print(f'The check in operation has taken {int(finish - start)} seconds so far.\n')
    if numberFinalPolicies == 0:
        # batch of 10
        checkOutInSummary["Check In Status"][-10:] = checkedInStatuses
    else:
        # not a batch of 10, checking the last batch of policies
        checkOutInSummary["Check In Status"][(-10 + (10 - numberFinalPolicies)):] = checkedInStatuses
    return checkOutInSummary

def write_to_csv(checkOutInSummary):
    checkOutInSummary = pd.DataFrame(checkOutInSummary)
    # # append the csv file - shouldn't be necessary since this script deletes the file if it exists at the beginning. The file should be created from scratch.
    # if os.path.isfile(filenameCsv):
    #     checkOutInSummary.to_csv(
    #         filenameCsv,
    #         header=False,
    #         index=False,
    #         na_rep="N/A",
    #         mode='a'
    #     )
    # Create the csv file and write data to it
    if os.path.isfile(filenameCsv):
        os.remove(filenameCsv)
    checkOutInSummary.to_csv(
        filenameCsv,
        index=False,
        na_rep="N/A"
    )

def main():
    start = time.perf_counter()
    parser = argparse.ArgumentParser(
        description="Automatically Check the Learning State for a Customer's WAF Policies."
    )
    # parser.add_argument("-custId", help="Check All Policies for Customer using Customer ID.")
    parser.parse_args()
    # delete file if it exists

    # custPolicies is json data (a list containing dictionaries wherein each dictionary is a WAF policy)
    custPolicies = get_customer_policies()
    checkOutInSummary = {
        "Policy": [],
        "Check Out Status": [],
        "Check Out Job Status URI": [],
        "Check In Status": [],
        "Check In Job Status URI": []
    }
    batchNumber = 1
    for index, policy in enumerate(custPolicies):
        policyName = f'{custId}-{policy["attributes"]["name"]}'
        policyPortalId = f'{policy["id"]}'
        if len(checkOutInSummary["Policy"]) < (10 * batchNumber):
            checkOutInSummary["Policy"].append(policyName)
            checkOutInSummary["Check Out Status"].append("IN PROGRESS")
            checkOutInSummary['Check Out Job Status URI'].append(check_out_policy(policyName, policyPortalId))
        if len(checkOutInSummary["Policy"]) == (10 * batchNumber):
            batchNumber += 1
            checkOutInSummary = check_out_status_check(checkOutInSummary)
            for policy in custPolicies[(len(checkOutInSummary["Policy"]) - 10):len(checkOutInSummary["Policy"])]:
                policyName = f'{custId}-{policy["attributes"]["name"]}'
                policyPortalId = f'{policy["id"]}'
                if checkOutInSummary["Check Out Status"][custPolicies.index(policy)] != "Completed":
                    # don't try to check in a policy that isn't checked out
                    checkOutInSummary["Check In Status"].append("Not attempted because policy failed check out.")
                    checkOutInSummary['Check In Job Status URI'].append('N/A')
                else:
                    # check out succeeded
                    checkOutInSummary["Check In Status"].append("IN PROGRESS")
                    checkOutInSummary['Check In Job Status URI'].append(check_in_policy(policyName, policyPortalId))
            checkOutInSummary = check_in_status_check(checkOutInSummary)
            write_to_csv(checkOutInSummary)
        elif index == (len(custPolicies) - 1):
            checkOutInSummary = check_out_status_check(checkOutInSummary)
            for policy in custPolicies[(len(checkOutInSummary["Policy"]) - (len(checkOutInSummary["Policy"]) - 10*(batchNumber-1))):len(checkOutInSummary["Policy"])]:
                policyName = f'{custId}-{policy["attributes"]["name"]}'
                policyPortalId = f'{policy["id"]}'
                if checkOutInSummary["Check Out Status"][custPolicies.index(policy)] != "Completed":
                    # don't try to check in a policy that isn't checked out
                    checkOutInSummary["Check In Status"].append("Not attempted because policy failed check out.")
                    checkOutInSummary['Check In Job Status URI'].append('N/A')
                else:
                    # check out succeeded
                    checkOutInSummary["Check In Status"].append("IN PROGRESS")
                    checkOutInSummary['Check In Job Status URI'].append(check_in_policy(policyName, policyPortalId))
            checkOutInSummary = check_in_status_check(checkOutInSummary)
            write_to_csv(checkOutInSummary)
    finish = time.perf_counter()
    totalTimeSec = round(finish - start, 2)
    totalTimeMin = round((finish - start)/60, 2)
    print(f"\nRuntime in seconds: {totalTimeSec}")
    print(f"Runtime in minutes: {totalTimeMin}")

main()