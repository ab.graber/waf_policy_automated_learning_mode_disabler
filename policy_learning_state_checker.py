# /usr/bin/env python3
# Avi Graber, F5 Networks, 2024-Jan-5
#
# This tool checks the learning state of each WAF policy for a given customer account/ID.

__author__ = "Avi Graber"
__date__ = "2024-Jan-5"
__version__ = "1.0"

import traceback
import requests
import datetime
import pandas as pd
import time
import sys
import os
import argparse
import urllib3
import getpass

# supressing SSL Warning from Policy Manager API cert
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
custId = input('Please type the customer ID (case-sensitive) and hit enter: ')
policyCheckedOutStatus = input(f"Have you confirmed that no {custId} WAF policies are currently checked out to either WAF editor (we17018.lon1 and we18018.lon1)? ").lower()
if policyCheckedOutStatus != 'y' and policyCheckedOutStatus != 'yes':
    sys.exit(f"Please confirm that no {custId} WAF policies are currently checked out to either WAF editor (we17018.lon1 and we18018.lon1) before proceeding with this task.")
# getpass library promotes security. The apiToken is not visible in the source code nor is it visible if someone were watching your terminal screen over your shoulder (shoulder surfing)
apiToken = getpass.getpass('Please copy-paste one of the customer\'s Portal API token values and hit enter: ')
date = str(datetime.datetime.today().date())
filenameCsv = f"./{custId}_learning_state_summary_{date}.csv"

def get_customer_policies():
    # retrieves list of WAF Policies from Policy Manager API for specified Customer ID
    # returns JSON response from Policy Manager API
    print(f"\nGetting List of Policies for {custId}\n")
    wipes = f"https://api.sjc1.defense.net/api/wipes/v2/customers/{custId}/policies"
    try:
        # verify=False means no validation of SSL cert
        responseWipes = requests.get(wipes, verify=False, timeout=10)
        # Policy Manager includes deleted policies. We're going to filter those out soon.
        dataWipesOriginal = responseWipes.json()
        if len(dataWipesOriginal["records"]) == 0:
            sys.exit(f"No Policies Found For: {custId}\nPlease Double Check the Customer ID")
    except requests.exceptions.Timeout:
        sys.exit("Request to Policy Manager API timed out. Are you connected to the VPN?")
    # Let's get the list of current WAF policies from the Silverline Portal API
    currentPolicies = []
    page = 1
    while page == 1 or len(currentPolicies) % 200 == 0:
        portalApiUri = f'https://portal.f5silverline.com/api/v1/waf_policies?page={page}'
        headers = {
            'Content-Type': 'application/json',
            'X-Authorization-Token': apiToken,
        }
        responsePortal = requests.get(portalApiUri, headers=headers, verify=False, timeout=10)
        if responsePortal.status_code != 200:
            sys.exit("Request to Portal API failed. Verify that your API token is valid.")
        dataPortal = responsePortal.json()
        wafDataPortal = dataPortal["data"]["waf_policies"]
        if len(wafDataPortal) == 0:
            break
        for policy in wafDataPortal:
            # The aforementioned Silverline Portal API endpoint contains only current WAF policies for the customer.
            currentPolicies.append(f'{custId}-{policy["attributes"]["name"]}')
        page += 1
    # Now, let's removed the deleted policies from the Policy Manager response by using the Portal API response.
    dataWipesFinal = {
        "records": []
    }
    for policy in dataWipesOriginal["records"]:
        if policy["name"] in currentPolicies and policy not in dataWipesFinal["records"]:
            dataWipesFinal["records"].append(policy)
    # dataWipesFinal now contains only current WAF policies for the customer. Deleted policies have been removed.
    return dataWipesFinal

def get_learning_state(policyJson):
    # processes WAF policy to determine learning state
    # returns JSON object of the results (policy name plus state of the 4 learning modules for the policy)
    learningState = {}
    policyBuilder = policyJson["policy"]["policy_builder"]
    try:
        learningState["policy_name"] = policyJson["name"]
    except:
        learningState["policy_name"] = "Error - could not identify policy name"
        # traceback.print_exc()
    learningModules = [
        "learn_from_responses",
        "learning_mode",
        "learn_inactive_entities",
        "enable_full_policy_inspection"
    ]
    for module in learningModules:
        try:
            learningState[module] = policyBuilder[module]
        except:
            if module == "learn_from_responses":
                # "learn_from_responses" is the oldest of the 4 learning modules.
                learningState[module] = "Does not exist in policy XML perhaps because last policy check-in is from 2015 or before"
                # traceback.print_exc()
            else:
                # The other 3 learning modules ("learning_mode", "learn_inactive_entities", and "enable_full_policy_inspection") weren't introduced in the Silverline Production environment until 2021.
                learningState[module] = "Does not exist in policy XML perhaps because last policy check-in is from 2020 or before"
    return learningState

def get_policy_from_wipes(policyId):
    # retrieves WAF Policy JSON from Policy Manager API
    wipes = f"https://api.sjc1.defense.net/api/wipes/v2/policies/{policyId}"
    try:
        response = requests.get(wipes, verify=False, timeout=10)
        data = response.json()
    except requests.exceptions.Timeout:
        sys.exit("Request to Policy Manager API timed out. Are you connected to the VPN?")
    return data

def check_all_customer_policies(custPolicies):
    # checks learning state for all WAF Policies for specified Customer ID
    print(f"Checking All {custId} Policies...\n")
    rateLimit = 0
    for policy in custPolicies["records"]:
        policyJson = get_policy_from_wipes(policy["id"])
        if rateLimit == 10:
            print("\nPROCESSED 10 POLICIES, PAUSING FOR 5 SECONDS\n")
            time.sleep(5)
            rateLimit = 0
        rateLimit += 1
        # learningState is json containing policy name and all 4 learning state modules
        learningState = get_learning_state(policyJson)
        # learningState will represent 1 row in the CSV file per policy.
        write_to_csv(learningState)
        print(f'{policy["name"]} - {policy["id"]}')

def write_to_csv(learningState):
    # columns is not cosmetic - names below must match learningState dictionary keys.
    columns = [
        "policy_name",
        "learn_from_responses",
        "learning_mode",
        "learn_inactive_entities",
        "enable_full_policy_inspection",
    ]
    # header is cosmetic. These are the column headers that will show in the CSV file.
    header = [
        "Policy",
        "Learn From Responses",
        "Learning Mode",
        "Learn Inactive Entities",
        "Enable Full Policy Inspection"
    ]
    learningState = pd.json_normalize(learningState)
    # append the csv file
    if os.path.isfile(filenameCsv):
        learningState.to_csv(
            filenameCsv,
            header=False,
            columns=columns,
            index=False,
            na_rep="N/A",
            mode='a'
        )
    if not os.path.isfile(filenameCsv):
        # Create the csv file and write data to it
        learningState.to_csv(
            filenameCsv,
            header=header,
            columns=columns,
            index=False,
            na_rep="N/A"
    )

def main():
    start = time.perf_counter()
    parser = argparse.ArgumentParser(
        description="Automatically Check the Learning State for a Customer's WAF Policies."
    )
    # parser.add_argument("-custId", help="Check All Policies for Customer using Customer ID.")
    parser.parse_args()
    # delete file if it exists
    if os.path.isfile(filenameCsv):
        os.remove(filenameCsv)
    # custPolicies is json data
    custPolicies = get_customer_policies()
    check_all_customer_policies(custPolicies)
    finish = time.perf_counter()
    totalTimeSec = round(finish - start, 2)
    totalTimeMin = round((finish - start)/60, 2)
    print(f"\nRuntime in seconds: {totalTimeSec}")
    print(f"Runtime in minutes: {totalTimeMin}")

main()