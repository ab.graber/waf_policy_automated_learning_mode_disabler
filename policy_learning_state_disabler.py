# /usr/bin/env python3
# Avi Graber, F5 Networks, 2024-Jan-5
#
# This tool disables the learning state of each WAF policy for a given customer account/ID.

__author__ = "Avi Graber"
__date__ = "2024-Jan-5"
__version__ = "1.0"

import traceback
import requests
import datetime
import pandas as pd
import time
import sys
import os
import argparse
import urllib3
import getpass

# supressing SSL Warning from Policy Manager API cert
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
custId = input('Please type the customer ID (case-sensitive) and hit enter: ')
policyCheckedOutStatus = input(f"Have you confirmed that no {custId} WAF policies are currently checked out to either WAF editor (we17018.lon1 and we18018.lon1)? ").lower()
if policyCheckedOutStatus != 'y' and policyCheckedOutStatus != 'yes':
    sys.exit(f"Please confirm that no {custId} WAF policies are currently checked out to either WAF editor (we17018.lon1 and we18018.lon1) before proceeding with this task.")
# getpass library promotes security. The apiToken is not visible in the source code nor is it visible if someone were watching your terminal screen over your shoulder (shoulder surfing)
apiToken = getpass.getpass('Please copy-paste one of the customer\'s Portal API token values and hit enter: ')
# Example portalUser is "John Doe"
portalUser = getpass.getpass("Please type your portal username (case-sensitive) and hit enter (typically the format is Firstname Lastname): ")
date = str(datetime.datetime.today().date())
filenameCsv = f"./{custId}_sync_policy_job_status_{date}.csv"

def get_customer_policies():
    # retrieves list of WAF Policies from Policy Manager API for specified Customer ID
    # returns JSON response from Policy Manager API
    print(f"\nGetting List of Policies for {custId}\n")
    wipes = f"https://api.sjc1.defense.net/api/wipes/v2/customers/{custId}/policies"
    try:
        # verify=False means no validation of SSL cert
        responseWipes = requests.get(wipes, verify=False, timeout=10)
        # Policy Manager includes deleted policies. We're going to filter those out soon.
        dataWipesOriginal = responseWipes.json()
        if len(dataWipesOriginal["records"]) == 0:
            sys.exit(f"No Policies Found For: {custId}\nPlease Double Check the Customer ID")
    except requests.exceptions.Timeout:
        sys.exit("Request to Policy Manager API timed out. Are you connected to the VPN?")
    # Let's get the list of current WAF policies from the Silverline Portal API
    currentPolicies = {
        "name": [],
        "portalId": []
    }
    page = 1
    while page == 1 or len(currentPolicies["name"]) % 200 == 0:
        portalApiUri = f'https://portal.f5silverline.com/api/v1/waf_policies?page={page}'
        headers = {
            'Content-Type': 'application/json',
            'X-Authorization-Token': apiToken,
        }
        responsePortal = requests.get(portalApiUri, headers=headers, verify=False, timeout=10)
        if responsePortal.status_code != 200:
            sys.exit("Request to Portal API failed. Verify that your API token is valid.")
        dataPortal = responsePortal.json()
        wafDataPortal = dataPortal["data"]["waf_policies"]
        if len(wafDataPortal) == 0:
            break
        for policy in wafDataPortal:
            # The aforementioned Silverline Portal API endpoint contains only current WAF policies for the customer.
            currentPolicies["name"].append(f'{custId}-{policy["attributes"]["name"]}')
            currentPolicies["portalId"].append(f'{policy["id"]}')
        page += 1
    # Now, let's removed the deleted policies from the Policy Manager response by using the Portal API response.
    dataWipesFinal = {
        "records": []
    }
    for policy in dataWipesOriginal["records"]:
        if policy["name"] in currentPolicies["name"] and policy not in dataWipesFinal["records"]:
            policy["portalId"] = currentPolicies["portalId"][currentPolicies["name"].index(policy["name"])]
            dataWipesFinal["records"].append(policy)
    # dataWipesFinal now contains only current WAF policies for the customer. Deleted policies have been removed.
    return dataWipesFinal

def disable_learning_state(policyJson):
    # The default is that we are not changing the policy.
    changeLearningState = 0
    policyBuilder = policyJson["policy"]["policy_builder"]
    # There are 4 learning modules
    learningModules = [
        "learn_from_responses",
        "learning_mode",
        "learn_inactive_entities",
        "enable_full_policy_inspection"
    ]
    for module in learningModules:
        print(f"Attempting to disable \"{module}\" module")
        time.sleep(1)
        try:
            # module needs to be disabled
            if policyBuilder[module] != "false" and policyBuilder[module] != "Disabled":
                # See "https://confluence.shpsec.com/display/PAO/Policy+Manager+-+Edit+whitelisted+XML+element" for reference
                urlEditElement = f'https://api.sjc1.defense.net/api/wipes/v2/policies/{policyJson["name"]}/edit-element'
                contentType = {'content-type': 'application/json'}
                xpath = f"/policy/policy_builder/{module}"
                # learning_mode is only module with desired value of Disabled. The others have desired value of false. These values are case-sensitive.
                payload = {
                    "xpath":xpath,
                    "value":"false"
                }
                if module == "learning_mode":
                    payload["value"] = "Disabled"
                response = requests.patch(urlEditElement, json=payload, headers=contentType, verify=False, timeout=10)
                if response.status_code == 200:
                    print("Success!")
                    print(response.json())
                    # The policy has been modified.
                    changeLearningState = 1
                    time.sleep(1)
                else:
                    print("Failed. You need to investigate this issue.")
                    print(response.json())
                    time.sleep(1)
            else:
                print(f"\"{module}\" module is already disabled.")
                time.sleep(1)
        except:
            if module == "learn_from_responses":
                print("No learning modules are present in policy XML perhaps because last policy check-in is from 2015 or before.")
                # traceback.print_exc()
            else:
                print(f"Modules:\n   \"{learningModules[1]}\"\n   \"{learningModules[2]}\" and\n   \"{learningModules[3]}\"\nare not present in policy XML perhaps because last policy check-in is from 2020 or before.")
            time.sleep(1)
            break
    return changeLearningState

def get_policy_from_wipes(policyId):
    # retrieves WAF Policy JSON from Policy Manager API
    wipes = f"https://api.sjc1.defense.net/api/wipes/v2/policies/{policyId}"
    try:
        response = requests.get(wipes, verify=False, timeout=10)
        data = response.json()
    except requests.exceptions.Timeout:
        print("Request to Policy Manager API timed out. Are you connected to the VPN?")
        sys.exit(1)
    return data

def sync_policy(policyJson, policyPortalId):
    # The below request details can be obtained from Portal deployment logs after a policy sync has been initiated. Note that initiating the sync from this script will not show in deployment logs, unlike initiating the sync from the Portal.
    uriDepUpdate = 'http://api.ss1-pdx.defense.net/api/v1/job-manager/jobs/dependency-update'
    policyName = policyJson["name"]
    headers = {
        'Content-Type': 'application/json',
        'X-Originator': 'customer-portal',
    }
    uriWipes = f'https://api.sjc1.defense.net/api/wipes/v2/policies/{policyName}/xml-by-name'
    jsonData = {
        'depupdate': {
            'name': policyName,
            'type': 'asm_policy',
            'uri': uriWipes,
            'version': -1,
            'customer_id': custId,
        },
        'job_meta': {
            'retriable': False,
            'originator': f'portal-ui:{portalUser}',
        },
        'pass_thru': {
            'customer_id': custId,
            'waf_policy_id': policyPortalId,
        },
        'originator': f'portal-ui:{portalUser}',
    }
    response = requests.post(uriDepUpdate, headers=headers, json=jsonData, verify=False, timeout=10)
    response = response.json()
    jobStatusUri = response["job_status_uri"]
    # jobStatusUri will be used to track the status of the policy sync operation (i.e. if the policy change has propagated to all BIT's where the policy exists).
    return jobStatusUri

def sync_status_check(modifiedPolicies, syncStatuses, jobStatusUris):
    start = time.perf_counter()
    finish = start
    # jobStatusUris contains 10 policy sync job status URI's
    while int(finish - start) < 900 and "IN PROGRESS" in syncStatuses:
        for index, jobStatusUri in enumerate(jobStatusUris):
            if syncStatuses[index] != "IN PROGRESS":
                continue
            response = requests.get(jobStatusUri, verify=False, timeout=10)
            response = response.json()
            if response["intention"]["success"] == True and response["intention"]["complete"] == True and response["intention"]["status"] == "complete":
                syncStatuses[index] = "Completed"
            elif response["intention"]["success"] == False and response["intention"]["complete"] == True and response["intention"]["status"] == "failed":
                syncStatuses[index] = "FAILED. Request the job status URI to investigate further."
        # check policy sync job statuses every 10 seconds
        time.sleep(5)
        print(f"{syncStatuses.count('Completed')} policies have successfully synced.")
        print(f"{syncStatuses.count('IN PROGRESS')} policies are still attempting to sync.")
        print(f"{syncStatuses.count('FAILED. Request the job status URI to investigate further.')} policies have failed to sync.")
        time.sleep(5)
        finish = time.perf_counter()
        print(f'The sync operation has taken {int(finish - start)} seconds so far.\n')
    csvSyncData = {
        "Policy": modifiedPolicies,
        "Sync Job Status": syncStatuses,
        "Job Status URI": jobStatusUris
    }
    write_to_csv(csvSyncData)

def write_to_csv(csvSyncData):
    csvSyncData = pd.DataFrame(csvSyncData)
    # append the csv file
    if os.path.isfile(filenameCsv):
        csvSyncData.to_csv(
            filenameCsv,
            header=False,
            index=False,
            na_rep="N/A",
            mode='a'
        )
    if not os.path.isfile(filenameCsv):
        # Create the csv file and write data to it
        csvSyncData.to_csv(
            filenameCsv,
            index=False,
            na_rep="N/A"
    )

def disable_learning_for_customer_policies(custPolicies):
    # disables learning state for all WAF Policies for specified Customer ID
    print(f"\nAttempting to disable learning state for {custId} Policies...")
    # modifiedPolicies is a list of policies that have had at least 1 learning module disabled as a result of this script
    modifiedPolicies = []
    # syncStatuses will track the status of syncing policy changes with Tier 2 infrastructure (i.e. propagating changes to BIT's)
    syncStatuses = []
    # jobStatusUris is a list of URI's we will use to track the status of sync operations for each modified policy.
    jobStatusUris = []
    for policy in custPolicies["records"]:
        policyJson = get_policy_from_wipes(policy["id"])
        policyPortalId = policy["portalId"]
        print(f'\n\n{policy["name"]} - {policy["id"]}')
        # If any learning module was disabled, changeLearningState will have a value of 1
        changeLearningState = disable_learning_state(policyJson)
        if changeLearningState == 1:
            modifiedPolicies.append(policy["name"])
            # The default sync status is in progress.
            syncStatuses.append("IN PROGRESS")
            jobStatusUris.append(sync_policy(policyJson, policyPortalId))
        if len(modifiedPolicies) == 10 or policy==custPolicies["records"][-1]:
            print(f'\nPROCESSED {len(modifiedPolicies)} POLICIES. CHECKING SYNC STATUS BEFORE CONTINUING TO PROCESS MORE POLICIES.\n')
            # We need to check the sync operation status for each of the 10 policies before moving forward to avoid overwhelming infrastructure.
            sync_status_check(modifiedPolicies, syncStatuses, jobStatusUris)
            # Let's reset the variables before moving to the next batch of 10 policies.
            modifiedPolicies = []
            syncStatuses = []
            jobStatusUris = []

def main():
    start = time.perf_counter()
    parser = argparse.ArgumentParser(
        description="Automatically Disable Learning State for a Customer's WAF Policies."
    )
    # parser.add_argument("-custId", help="Disable learning state for Customer using Customer ID.")
    parser.parse_args()
    # delete file if it exists
    if os.path.isfile(filenameCsv):
        os.remove(filenameCsv)
    # custPolicies is json data
    custPolicies = get_customer_policies()
    disable_learning_for_customer_policies(custPolicies)
    finish = time.perf_counter()
    totalTimeSec = round(finish - start, 2)
    totalTimeMin = round((finish - start)/60, 2)
    print(f"\nRuntime in seconds: {totalTimeSec}")
    print(f"Runtime in minutes: {totalTimeMin}")

main()