# README #

This README documents the steps necessary to get the scripts in this repository up and running on your local machine.

1. https://support.f5silverline.com/hc/en-us/articles/20786256172055-SOCDOC-How-to-Disable-Learning-Mode-for-all-WAF-Policies-in-a-Customer-Account (This is the foundational process article. The below two articles are extensions of this foundational process article.)
2. https://support.f5silverline.com/hc/en-us/articles/22389336453143-SOCDOC-Why-is-WAF-Policy-Learning-Mode-Problematic-for-Silverline-Infrastructure (This article provides conceptual background information on the project.)
3. https://support.f5silverline.com/hc/en-us/articles/22389412669847-SOCDOC-How-Do-I-Perform-Passed-Violation-Analysis-for-Hotlist-Customers-Before-Disabling-Learning-Mode-on-their-WAF-Policies (This article is a direct response to https://jira.shpsec.com/browse/ESC-81984.)